<?php
//fungsi ini dengan return value, &  parameter
function cetak_ganjil($awal,$akhir){
    for ($i=$awal; $i <$akhir; $i++) { 
        if ($i%2==1) {
            echo "$i,";
        }
    }
}
//pemanggilan fungsi
$a = 20;
$b = 60;
echo "<b>Bilangan Ganjil dari $a sampai $b, adalah : </b><br>";
cetak_ganjil($a,$b);
?>