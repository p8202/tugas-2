<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TUGAS AKHIR</title>
    <link rel="stylesheet" type="text/css" href="style_about.css">
</head>
<body>
    <header>
        <a href="index.html" class="logo">RUMAH MAKAN TIDAK SEDERHANA</a>
        <div class="menu-toggle"></div>
        <nav>
            <ul>
                <li><a href="index.html" class="">Home</a><li>
                <li><a href="about.php" class="active">About</a><li>
                <li><a href="product.html" class="">Product</a><li>
                <li><a href="bukutamu.php" class="">Buku Tamu</a><li>

            </ul>
        </nav>
        <div class="clearfix"></div>
    </header>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".menu-toggle").click(function(){
                $(".menu-toggle").toggleClass("active")
                $("nav").toggleClass("active")
            })
        })
    </script>
    <div class="container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63245.97077191183!2d110.33982529872057!3d-7.80324901040523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5787bd5b6bc5%3A0x21723fd4d3684f71!2sYogyakarta%2C%20Kota%20Yogyakarta%2C%20Daerah%20Istimewa%20Yogyakarta!5e0!3m2!1sid!2sid!4v1658138631751!5m2!1sid!2sid" width="1500" height="1000" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</head>
<body>
    <div class="main-content">
        <div class="left box">
            <h2>Tentang Website</h2>
            <div class="content">
                <p>Website ini menjual berbagai makanan seperti Rendang Daging, Cumi Pedas Saus Tiram, Dendeng Bathokok Lado, Gulai Duan Singkong, Ikan Tenggiri Asam Pedas, 
                    Jengkol Balado, Tauco Sayur Pedas, Tongkol Jontor, Udang Pedas Manis.</p>
                <div class="centerbox">
                    <h2>Lokasi</h2>
                    <div class="content">
                    <div class="place">
                    <span class="fas fa-map-marker-alt"></span>
                    <span class="text">Kec. Umbulharjo., Kota Yogyakarta, Daerah Istimewa Yogyakarta</span>
                </div>
                <h2>Telepon</h2>
                <div class="phone">
                    <span class="fas fa-phone-alt"></span>
                    <span class="text">(+62)813 7972 6288</span>
                <h2>Email</h2>
                <div class="email">
                    <span class="text">efanilham16@gmail.com</span>
                <div class="medsos">
                    <ul>
                        <li><a href="#"><img src="fb.png" width="25px" height="25px"></a></li>
			            <li><a href="#"><img src="tw.png" width="25px" height="25px"></a></li>
			            <li><a href="#"><img src="ig.png" width="25px" height="25px"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footerbottom">
        <div class="copyright">
            <h4 style="margin-top: 0px;"><br>Muhammad Efani Ilham Copyright&copy 2022</br></h4>
        </div>
    </div>
</footer>
</html>